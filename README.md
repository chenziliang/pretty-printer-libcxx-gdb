## Pretty printer for libcxx in GDB

cp `gdbinit` to `~/.gdbinit` and revise the python path in it to point to `pretty-printer-libcxx-gdb/src`

Prefer to use llvm version pretty printer as the orig one has bugs for shared_ptr

## References

https://github.com/koutheir/libcxx-pretty-printers

https://github.com/llvm/llvm-project/blob/master/libcxx/utils/gdb/libcxx/printers.py

